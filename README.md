# Hotmart Challenge

Desafio para Desenvolvedor Java:


- Java 11
- [Spring Boot na versão 2.4.2](https://spring.io/projects/spring-boot)
- [Maven na versão 3.6.3](https://maven.apache.org/)
- [JDK 11 (Corretto)](https://docs.aws.amazon.com/corretto/latest/corretto-11-ug/downloads-list.html)
- [IntelliJ](https://www.jetbrains.com/pt-br/idea/)

## Executando o programa localmente

Existem diversas maneiras de executar um aplicativo Spring Boot em sua máquina local.
Execute o seguinte comando do maven para baixar as dependências:

`mvn clean install`

Uma maneira é executar o método `main` no caminho `com.hotmart.challenge.ChallengeApplication.java` de seu IDE.

## Documentação

- [Open API Swagger](https://gitlab.com/lucas.misson/desafio-hotmart/-/blob/master/swagger.json)

- [Postman](https://gitlab.com/lucas.misson/desafio-hotmart/-/blob/master/postman_collection.json)
