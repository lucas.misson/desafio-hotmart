package com.hotmart.challenge.config.swagger;

import com.hotmart.challenge.model.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

@Configuration
public class SwaggerConfiguration {

    @Value("${hotmart.swagger.titulo}")
    private String swaggerTitulo;

    @Value("${hotmart.swagger.descricao}")
    private String swaggerDescricao;

    @Value("${hotmart.swagger.nome}")
    private String swaggerNome;

    @Value("${hotmart.swagger.site}")
    private String swaggerSite;

    @Value("${hotmart.swagger.email}")
    private String swaggerEmail;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.hotmart.challenge.controller"))
                .paths(PathSelectors.any())
                .build()
                .ignoredParameterTypes(User.class)
                .apiInfo(apiInfo())
                .globalOperationParameters(
                        Arrays.asList(
                                new ParameterBuilder()
                                        .name("Authorization")
                                        .description("Header - JWT Token")
                                        .modelRef(new ModelRef("string"))
                                        .parameterType("header")
                                        .required(false)
                                        .build()));
    }

    private ApiInfo apiInfo() {

        @SuppressWarnings("rawtypes")
        final Collection<VendorExtension> vendorExtensions = new ArrayList<>();

        swaggerTitulo = new String(swaggerTitulo.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
        swaggerDescricao = new String(swaggerDescricao.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
        swaggerNome = new String(swaggerNome.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);

        return new ApiInfo(
                swaggerTitulo,
                swaggerDescricao,
                "1.0.0",
                "",
                new Contact(swaggerNome, swaggerSite, swaggerEmail),
                "",
                "",
                vendorExtensions);
    }
}
