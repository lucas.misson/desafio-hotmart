package com.hotmart.challenge.config.security;

import com.hotmart.challenge.model.User;
import com.hotmart.challenge.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userLogin) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByEmail(userLogin);
        if(user.isPresent()) {
            return user.get();
        }

        throw new UsernameNotFoundException("Invalid user!");
    }
}
