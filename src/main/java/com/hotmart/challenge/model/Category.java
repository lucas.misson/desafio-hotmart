package com.hotmart.challenge.model;

import io.github.ccincharge.newsapi.datamodels.Article;
import lombok.Data;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Audited
@Data
@Entity
@AuditTable(value = "category_audit")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull @NotEmpty
    private String name;

    @OneToMany
    @JoinColumn(name = "news_id")
    private List<News> news;

    public Category() {
    }

    public Category(@NotNull @NotEmpty String name, List<News> categoryNews) {
        this.name = name;
        this.news = categoryNews;
    }
}
