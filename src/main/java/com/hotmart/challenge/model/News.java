package com.hotmart.challenge.model;

import lombok.Data;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Audited
@Data
@Entity
@AuditTable(value = "news_audit")
public class News {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "author", columnDefinition = "LONGTEXT")
    private String author;

    @Column(name = "title", columnDefinition = "LONGTEXT")
    private String title;

    @Column(name = "description", columnDefinition = "LONGTEXT")
    private String description;

    public News() {
    }

    public News(@NotNull @NotEmpty String author, @NotNull @NotEmpty String title,
                @NotNull @NotEmpty String description) {
        this.author = author;
        this.title = title;
        this.description = description;
    }
}
