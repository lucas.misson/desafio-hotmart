package com.hotmart.challenge.model;

import lombok.Data;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Audited
@Data
@Entity(name = "SALE")
@AuditTable(value = "sale_audit")
public class Sale {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "saller_id", nullable = false)
    private Saller saller;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "buyer_id", nullable = false)
    private Buyer buyer;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false)
    private Product product;

    private LocalDateTime saleDate = LocalDateTime.now();

    private Double evaluation;

    public Sale() {
    }

    public Sale(@NotNull Saller saller, @NotNull Buyer buyer, @NotNull Product product, Double evaluation) {
        this.saller = saller;
        this.buyer = buyer;
        this.product = product;
        this.evaluation = evaluation;
    }
}
