package com.hotmart.challenge.scheduled;

import com.hotmart.challenge.model.Category;
import com.hotmart.challenge.model.News;
import com.hotmart.challenge.model.Product;
import com.hotmart.challenge.repository.CategoryRepository;
import com.hotmart.challenge.repository.NewsRepository;
import com.hotmart.challenge.repository.ProductRepository;
import com.hotmart.challenge.repository.SaleRepository;
import io.github.ccincharge.newsapi.NewsApi;
import io.github.ccincharge.newsapi.datamodels.Article;
import io.github.ccincharge.newsapi.requests.RequestBuilder;
import io.github.ccincharge.newsapi.responses.ApiArticlesResponse;
import io.github.ccincharge.newsapi.responses.ApiSourcesResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductsScoreScheduled {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private SaleRepository saleRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private NewsRepository newsRepository;

    @Scheduled(initialDelay=0, fixedDelayString = "${hotmart.populate-news}")
    @Transactional
    private void getNewsApi() {
        NewsApi newsApi = new NewsApi("68fb7a2b6d2342ce9ce572878e260139");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        RequestBuilder worldNoticesRequest = new RequestBuilder()
                .setQ("World")
                .setFrom(LocalDate.now().format(formatter));

        ApiArticlesResponse apiArticle = newsApi.sendTopRequest(worldNoticesRequest);
        ApiSourcesResponse apiSources = newsApi.sendSourcesRequest(worldNoticesRequest);

        insertNews(apiArticle);
        insertCategories(apiArticle, apiSources);
    }

    @Scheduled(initialDelay=15000, fixedDelayString = "${hotmart.fill-daily-ranking}")
    @Transactional
    private void fillScore() {
        List<Product> products = productRepository.findAll();

        getProductScore(products);
    }

    private void getProductScore(List<Product> products) {
        products.forEach(p -> {
            Double evaluation = getProductsEvaluationAverage(p);
            Double salesRatioByDays = getSalesRatioByDays(p);
            Double numberCategories = getNumberOfProductCategory(p);

            p.setRanking(evaluation + salesRatioByDays + numberCategories);
            productRepository.save(p);
        });
    }

    private Double getProductsEvaluationAverage(Product p) {
        Double evaluationAvg = saleRepository.findAverageProductEvaluation(LocalDate.now().minusYears(1), p.getId());
        return evaluationAvg != null ? evaluationAvg : 0.0;
    }

    private Double getSalesRatioByDays(Product p) {
        Integer numberSales = saleRepository.countByProductId(p.getId());
        Long productDays = Duration.between(p.getCreationDate(), LocalDateTime.now()).toDays();
        return (numberSales != null ? numberSales : 0.0) / (productDays);
    }

    private Double getNumberOfProductCategory(Product p) {
        return productRepository.countByCategoryNewsId(p.getId());
    }

    private void insertNews(ApiArticlesResponse apiArticle) {
        List<News> allNews = new ArrayList<>();
        apiArticle.articles().forEach(a -> {
            Optional<News> news = newsRepository.findByAuthorAndTitleAndDescription(
                    a.author(), a.title(), a.description());
            if(!news.isPresent()) {
                allNews.add(new News(a.author(), a.title(), a.description()));
            }
        });

        newsRepository.saveAll(allNews);
        newsRepository.flush();
    }

    private void insertCategories(ApiArticlesResponse apiArticle, ApiSourcesResponse apiSources) {
        List<Category> allCategories = new ArrayList<>();
        apiSources.sources().forEach(s -> {
            Optional<Category> category = categoryRepository.findByName(s.name());
            if(!category.isPresent()) {
                List<Article> articles = apiArticle.articles().stream()
                        .filter(n -> s.name().equals(n.source().description()))
                        .collect(Collectors.toList());
                List<News> categoryNews = new ArrayList<>();
                articles.forEach(a -> {
                    categoryNews.add(new News(a.author(), a.title(), a.description()));
                });
                allCategories.add(new Category(s.name(), categoryNews));
            }
        });

        categoryRepository.saveAll(allCategories);
        categoryRepository.flush();
    }

}
