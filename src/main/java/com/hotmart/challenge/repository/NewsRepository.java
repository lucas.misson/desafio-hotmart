package com.hotmart.challenge.repository;

import com.hotmart.challenge.model.News;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface NewsRepository extends JpaRepository<News, Long> {

    Optional<News> findByAuthorAndTitleAndDescription(String author, String title, String description);

}
