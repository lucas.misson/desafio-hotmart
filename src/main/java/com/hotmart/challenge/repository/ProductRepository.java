package com.hotmart.challenge.repository;

import com.hotmart.challenge.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {

    Page<Product> findByNameContainingIgnoreCase(
            String searchField, Pageable pagination);

    Double countByCategoryNewsId(Long p);
}
