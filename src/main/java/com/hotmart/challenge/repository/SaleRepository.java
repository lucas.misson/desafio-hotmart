package com.hotmart.challenge.repository;

import com.hotmart.challenge.dto.ScoreProductDto;
import com.hotmart.challenge.model.Sale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface SaleRepository extends JpaRepository<Sale, Long> {

    @Query(value = "SELECT AVG(s.evaluation) FROM Sale s WHERE s.sale_date >= :data AND s.product_id = :prodId", nativeQuery = true)
    Double findAverageProductEvaluation(@Param("data") LocalDate lastYeardate,
                                        @Param("prodId")Long productId);

    Integer countByProductId(Long id);
}
