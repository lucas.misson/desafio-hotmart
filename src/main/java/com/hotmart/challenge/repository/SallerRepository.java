package com.hotmart.challenge.repository;

import com.hotmart.challenge.model.Saller;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SallerRepository extends JpaRepository <Saller, Long>{
}
