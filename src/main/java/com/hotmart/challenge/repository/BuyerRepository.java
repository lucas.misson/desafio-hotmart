package com.hotmart.challenge.repository;

import com.hotmart.challenge.model.Buyer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BuyerRepository extends JpaRepository<Buyer, Long> {
}
