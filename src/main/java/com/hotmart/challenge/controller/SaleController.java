package com.hotmart.challenge.controller;

import com.hotmart.challenge.dto.SaleDto;
import com.hotmart.challenge.form.SaleEvaluationForm;
import com.hotmart.challenge.form.SaleForm;
import com.hotmart.challenge.model.Buyer;
import com.hotmart.challenge.model.Product;
import com.hotmart.challenge.model.Sale;
import com.hotmart.challenge.model.Saller;
import com.hotmart.challenge.repository.BuyerRepository;
import com.hotmart.challenge.repository.ProductRepository;
import com.hotmart.challenge.repository.SaleRepository;
import com.hotmart.challenge.repository.SallerRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Optional;

@RestController
@RequestMapping("/sales")
@Api(value = "Sale", tags = {"Operations with sale"})
public class SaleController {

    @Autowired
    private SaleRepository saleRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private BuyerRepository buyerRepository;

    @Autowired
    private SallerRepository sallerRepository;

    @ApiOperation(value = "Sales List")
    @GetMapping()
    public Page<SaleDto> salesList(Pageable pagination) {
        return SaleDto.converter(saleRepository.findAll(pagination));
    }

    @ApiOperation(value = "Insert Sale")
    @PostMapping
    @Transactional
    public ResponseEntity<SaleDto> insertSale(
            @ApiParam(value = "Sale Object", required = true)
            @RequestBody @Valid SaleForm form, UriComponentsBuilder uriBuilder) {

        Optional<Saller> sallerOptional = sallerRepository.findById(form.getSaller());
        Optional<Buyer> buyerOptional = buyerRepository.findById(form.getBuyer());
        Optional<Product> productOptional = productRepository.findById(form.getProduct());
        if(sallerOptional.isPresent() && buyerOptional.isPresent() && productOptional.isPresent()) {
            Sale sale = form.converter(
                    sallerOptional.get(), buyerOptional.get(), productOptional.get(), form.getEvaluation());

            saleRepository.save(sale);

            URI uri = uriBuilder.path("/sales/{id}").buildAndExpand(sale.getId()).toUri();
            return ResponseEntity.created(uri).body(new SaleDto(sale));
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(value = "Product Evaluation")
    @PutMapping("/{id}")
    @Transactional
    public ResponseEntity<SaleDto> updateProduct(
            @ApiParam(value = "Sale Id", required = true)
            @PathVariable("id") Long id,
            @ApiParam(value = "Evaluation", required = true)
            @RequestBody @Valid SaleEvaluationForm form) {
        Optional<Sale> saleOptional = saleRepository.findById(id);
        if (saleOptional.isPresent()) {
            saleOptional.get().setEvaluation(form.getProductEvaluation());
            saleRepository.saveAndFlush(saleOptional.get());

            return ResponseEntity.ok(new SaleDto(saleOptional.get()));
        }
        return ResponseEntity.notFound().build();
    }
}
