package com.hotmart.challenge.controller;

import com.hotmart.challenge.dto.ProductDto;
import com.hotmart.challenge.form.ProductForm;
import com.hotmart.challenge.model.Category;
import com.hotmart.challenge.model.Product;
import com.hotmart.challenge.repository.CategoryRepository;
import com.hotmart.challenge.repository.ProductRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Optional;

@RestController
@RequestMapping("/products")
@Api(value = "Product", tags = {"Operations with products"})
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @ApiOperation(value = "Products List")
    @GetMapping
    public Page<ProductDto> productsList(
            @ApiParam(value = "search field", required = false)
            @RequestParam(required = false) String searchField,
            Pageable pagination
    ) {
        if (searchField == null) {
            return ProductDto.converter(productRepository.findAll(pagination));
        }
        return ProductDto.converter(
                productRepository.findByNameContainingIgnoreCase(
                        searchField, pagination));
    }

    @ApiOperation(value = "Insert Product")
    @PostMapping
    @Transactional
    public ResponseEntity<ProductDto> insertProduct(
            @ApiParam(value = "Product Object", required = true)
            @RequestBody @Valid ProductForm form, UriComponentsBuilder uriBuilder) {

        Optional<Category> category = categoryRepository.findByName(form.getCategoryName());
        if(category.isPresent()) {
            Product product = form.converter(category.get());
            productRepository.save(product);

            URI uri = uriBuilder.path("/products/{id}").buildAndExpand(product.getId()).toUri();
            return ResponseEntity.created(uri).body(new ProductDto(product));
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(value = "Find Product By Id")
    @GetMapping("/{id}")
    public ResponseEntity<ProductDto> detailProduct(
            @ApiParam(value = "Product Id", required = true)
            @PathVariable("id") Long id) {
        Optional<Product> product = productRepository.findById(id);
        if(product.isPresent()) {
            return ResponseEntity.ok(new ProductDto(product.get()));
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(value = "Change Product")
    @PutMapping("/{id}")
    @Transactional
    public ResponseEntity<ProductDto> updateProduct(
            @ApiParam(value = "Product Id", required = true)
            @PathVariable("id") Long id,
            @ApiParam(value = "Product Object", required = true)
            @RequestBody @Valid ProductForm form) {
        Optional<Product> produtoOptional = productRepository.findById(id);
        Optional<Category> categoryOptional = categoryRepository.findByName(form.getCategoryName());
        if(produtoOptional.isPresent() && categoryOptional.isPresent()) {
            Product product = form.update(id, produtoOptional.get(), categoryOptional.get());

            return ResponseEntity.ok(new ProductDto(product));
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(value = "Remove Product")
    @DeleteMapping("/{id}")
    @Transactional
    public ResponseEntity<?> removeProduct(
            @ApiParam(value = "Product Id", required = true)
            @PathVariable("id") Long id) {
        Optional<Product> product = productRepository.findById(id);
        if(product.isPresent()) {
            productRepository.deleteById(id);

            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }

}
