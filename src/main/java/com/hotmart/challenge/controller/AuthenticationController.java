package com.hotmart.challenge.controller;

import com.hotmart.challenge.config.security.TokenService;
import com.hotmart.challenge.dto.TokenDto;
import com.hotmart.challenge.form.LoginForm;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
@Api(value = "Authentication", tags = {"Operations with authentication"})
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenService tokenService;

    @PostMapping
    @ApiOperation(value = "Login auth")
    public ResponseEntity<TokenDto> authenticate(
            @ApiParam(value = "login values", required = true)
            @RequestBody @Valid LoginForm form) {

        UsernamePasswordAuthenticationToken loginInfo = form.converter();

        try {
            Authentication authentication = authenticationManager.authenticate(loginInfo);
            String token = tokenService.generateToken(authentication);
            return ResponseEntity.ok(new TokenDto(token, "Baerer"));
        } catch (AuthenticationException e) {
            return ResponseEntity.badRequest().build();
        }

    }

}
