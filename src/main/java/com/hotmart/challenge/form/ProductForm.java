package com.hotmart.challenge.form;

import com.hotmart.challenge.model.Category;
import com.hotmart.challenge.model.Product;
import com.sun.istack.NotNull;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class ProductForm {

    @NotNull @NotEmpty
    private String name;
    @NotNull @NotEmpty
    private String description;
    @NotNull @NotEmpty
    private String categoryName;

    public Product converter(Category category) {
        return new Product(this.name, this.description, category);
    }

    public Product update(Long id, Product product, Category category) {
        product.setName(this.name);
        product.setDescription(this.description);
        product.setCategory(category);

        return product;
    }
}
