package com.hotmart.challenge.form;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
public class SaleEvaluationForm {

    @NotNull @Min(0) @Max(5)
    private Double productEvaluation;

}
