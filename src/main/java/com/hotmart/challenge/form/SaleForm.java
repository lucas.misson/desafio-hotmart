package com.hotmart.challenge.form;

import com.hotmart.challenge.model.*;
import com.sun.istack.NotNull;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
public class SaleForm {

    @NotNull
    private Long saller;

    @NotNull
    private Long buyer;

    @NotNull
    private Long product;

    @NotNull @Min(0) @Max(5)
    private Double evaluation;

    public Sale converter(Saller saller, Buyer buyer, Product product, Double evaluation) {
        return new Sale(saller, buyer, product, evaluation);
    }
}
