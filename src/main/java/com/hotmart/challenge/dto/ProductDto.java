package com.hotmart.challenge.dto;

import com.hotmart.challenge.model.Product;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.time.LocalDateTime;

@Data
public class ProductDto {

    private Long id;
    private String name;
    private String description;
    private LocalDateTime creationDate;
    private Double score;

    public ProductDto(Product product) {
        this.id = product.getId();
        this.name = product.getName();
        this.description = product.getDescription();
        this.creationDate = product.getCreationDate();
        this.score = product.getRanking();
    }
    public static Page<ProductDto> converter(Page<Product> products) {
        return products.map(ProductDto::new);
    }
}
