package com.hotmart.challenge.dto;

import lombok.Data;

@Data
public class ScoreProductDto {

    private Long productId;
    private Float averageRating;
    private Double salesRatioByDays;
    private Integer numberOfNotices;
}
