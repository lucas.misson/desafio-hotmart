package com.hotmart.challenge.dto;

import com.hotmart.challenge.model.Sale;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
public class SaleDto {

    private Long id;
    private String productName;
    private Double evaluation;

    public SaleDto(Sale sale) {
        this.id = sale.getId();
        this.productName = sale.getProduct().getName();
        this.evaluation = sale.getEvaluation() != null ? sale.getEvaluation() : 0.0;
    }

    public static Page<SaleDto> converter(Page<Sale> sales) {
        return sales.map(SaleDto::new);
    }
}
